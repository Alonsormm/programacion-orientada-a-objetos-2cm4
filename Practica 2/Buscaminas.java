import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.event.ActionEvent;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;


class Buscaminas extends JFrame implements ActionListener{
    private static final long serialVersionUID = 1L;
    Casilla tableroMinas[][];
    JButton tableroGrafico[][];
    JLabel juego;
    JPanel malla[];
    JFrame ventana;
    ImageIcon minaImagen;
    java.net.URL imgUrl;
    public Buscaminas(){
        malla = new JPanel[10];
        for(int i = 0 ; i < 10; i++)
            malla[i] = new JPanel();

        ventana = new JFrame();

        tableroMinas = new Casilla[10][10];
        for(int i = 0 ; i < 10; i++){
            for(int j = 0 ; j < 10; j++){
                tableroMinas[i][j] = new Casilla();
            }
        }
        
        juego = new JLabel();

        imgUrl = getClass().getResource("blanco.png");
        minaImagen = new ImageIcon(imgUrl);


        tableroGrafico = new JButton[10][10];
        for(int i = 0 ; i < 10; i++){
            for(int j = 0 ; j < 10; j++){
                minaImagen = new ImageIcon(imgUrl);
                tableroGrafico[i][j] = new JButton(minaImagen);
                tableroGrafico[i][j].addActionListener(this);
            }
        }
        for(int i = 0; i < 10; i++){
            malla[i].setLayout(new FlowLayout());
            malla[i].setSize(200,100);
        }
        for(int i = 0 ; i < 10; i++){
            for(int j = 0 ; j < 10; j++){
                malla[i].add(tableroGrafico[i][j]);
            }
        }
                
        ventana.setLayout(new FlowLayout());
        for(int i = 0 ; i < 10; i++)
            ventana.add(malla[i]);
        ventana.add(juego);
        ventana.setSize(900,700);
        ventana.setVisible(true);

        
    }
    public int calcularCasilla(Casilla minaTemp[][]){
        
        return 0;
    }
    public void imprimirTablero(){
        String l;
        for(int i  = 0 ; i < 10 ; i++){
            for(int j = 0 ; j < 10; j++){
                if(tableroMinas[i][j].mina){
                    imgUrl = getClass().getResource("AsiMejor.png");
                    minaImagen = new ImageIcon(imgUrl);
                    tableroGrafico[i][j].setIcon(minaImagen);
                }
                else{
                    int cnt = 0;
                    if(i<9){
                        if(tableroMinas[i+1][j].mina)
                            cnt++;
                        if(j<9)
                            if(tableroMinas[i+1][j+1].mina)
                                cnt++;
                        if(j>0)
                            if(tableroMinas[i+1][j-1].mina)
                                cnt++;
                    }
                    if(j>0)
                        if(tableroMinas[i][j-1].mina)
                            cnt++;
                    if(j<9)                        
                        if(tableroMinas[i][j+1].mina)
                            cnt++;
                    if(i>0){
                        if(tableroMinas[i-1][j].mina)
                            cnt++;
                        if(j<9)
                            if(tableroMinas[i-1][j+1].mina)
                                cnt++;
                        if(j>0)
                            if(tableroMinas[i-1][j-1].mina)
                                cnt++;
                    }
                    l = Integer.toString(cnt);
                    imgUrl = getClass().getResource(l +".png");
                    if(cnt == 0){
                        imgUrl = getClass().getResource("blanco.png");
                    }
                    minaImagen = new ImageIcon(imgUrl);
                    tableroGrafico[i][j].setIcon(minaImagen);
                }
            }
        }
    }
    public void actionPerformed(ActionEvent e){
        String l;
        for(int i = 0 ; i < 10; i++){
            for(int j = 0 ; j < 10; j++){
                if(e.getSource() == tableroGrafico[i][j]){
                    if(tableroMinas[i][j].mina){
                        imprimirTablero();
                    }
                    else{
                        int cnt = 0;
                        if(i<9){
                            if(tableroMinas[i+1][j].mina)
                                cnt++;
                            if(j<9)
                                if(tableroMinas[i+1][j+1].mina)
                                    cnt++;
                            if(j>0)
                                if(tableroMinas[i+1][j-1].mina)
                                    cnt++;
                        }
                        if(j>0)
                            if(tableroMinas[i][j-1].mina)
                                cnt++;
                        if(j<9)                        
                            if(tableroMinas[i][j+1].mina)
                                cnt++;
                        if(i>0){
                            if(tableroMinas[i-1][j].mina)
                                cnt++;
                            if(j<9)
                                if(tableroMinas[i-1][j+1].mina)
                                    cnt++;
                            if(j>0)
                                if(tableroMinas[i-1][j-1].mina)
                                    cnt++;
                        }
                        l = Integer.toString(cnt);
                        imgUrl = getClass().getResource(l +".png");
                        if(cnt == 0){
                            imgUrl = getClass().getResource("blanco.png");
                        }
                        minaImagen = new ImageIcon(imgUrl);
                        tableroGrafico[i][j].setIcon(minaImagen);;
                    }
                }
            }
        }
    }
    public static void main(String[] args) {
        new Buscaminas();
    }
}