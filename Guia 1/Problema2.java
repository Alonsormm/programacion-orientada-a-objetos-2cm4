import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.event.ActionEvent;

public class Problema2 extends JFrame implements ActionListener{
    JButton comparar;
    JTextField text1, text2;
    JLabel resultado;
    Container c;
    public Problema2(){
        c = getContentPane();
        comparar = new JButton("Compara");
        text1 = new JTextField();
        text2 = new JTextField();
        resultado = new JLabel("**********");
        comparar.addActionListener(this);
        c.setLayout(new BorderLayout());
        add("North",text1);
        add("South",text2);
        add("East", comparar);
        add("West", resultado);
        setSize(500,100);
        setVisible(true);
    }
    public void actionPerformed(ActionEvent e){
        String sText1, sText2;
        sText1 = text1.getText();
        sText2 = text2.getText();
        if(sText1.equals(sText2))
            resultado.setText("Son iguales");
        else
            resultado.setText("Son diferentes");

    }
    public static void main(String[] args) {
        new Problema2();
    }  
}