import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.event.ActionEvent;

public class Problema1 extends JFrame implements ActionListener{
    JButton DolltPes, PestDoll;
    JTextField Dollars,Pesos;
    Container c;
    public Problema1(){
        c = getContentPane();
        DolltPes = new JButton("Dolares a Pesos");
        PestDoll = new JButton("Pesos a Dolares");
        Dollars = new JTextField("Dollars");
        Pesos = new JTextField();
        DolltPes.addActionListener(this);
        PestDoll.addActionListener(this);
        c.setLayout(new BorderLayout());
        add("North",Dollars);
        add("West", DolltPes);
        add("East", PestDoll);
        add("South",Pesos);
        setSize(500,100);
        setVisible(true);
    }
    public void actionPerformed(ActionEvent e){
        float dolaresI;
        float  pesosI;
        String resultado;
        String temp;
        if(e.getSource() == DolltPes){
            temp = Dollars.getText();
            pesosI = Float.parseFloat(temp)*20;
            resultado = Float.toString(pesosI);
            Pesos.setText(resultado);
        }
        else{
            temp = Pesos.getText();
            dolaresI = Float.parseFloat(temp)/20;
            resultado = Float.toString(dolaresI);
            Dollars.setText(resultado);
        }
    }
    public static void main(String[] args) {
        new Problema1();
    }
}