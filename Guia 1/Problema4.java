import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class Problema4 extends JFrame implements ActionListener{
    JButton suma,resta,multiplicacion,division;
    JTextField oper1, oper2;
    JLabel resultadoFinal;
    JPanel arriba, abajo; 
    JFrame total;
    public Problema4(){
        total = new JFrame();
        abajo = new JPanel();
        arriba = new JPanel();
        suma = new JButton("Suma");
        resta = new JButton("Resta");
        multiplicacion = new JButton("Multiplicacion");
        division = new JButton("Division");
        oper1 = new JTextField();
        oper2 = new JTextField();
        resultadoFinal = new JLabel("=   ");
        suma.addActionListener(this);
        resta.addActionListener(this);
        multiplicacion.addActionListener(this);
        division.addActionListener(this);

        
        total.setLayout(new BorderLayout());
        abajo.setLayout(new FlowLayout());
        arriba.setLayout(new BorderLayout());

        total.add("North",arriba);
        total.add("South",abajo);

        arriba.add("North",oper1);
        arriba.add("South",oper2);


        abajo.add(suma);
        abajo.add(resta);
        abajo.add(multiplicacion);
        abajo.add(division);
        abajo.add(resta);
        abajo.add(resultadoFinal);

        arriba.setSize(400,50);
        abajo.setSize(400,50);


        total.setSize(500,120);
        total.setVisible(true);
    }
    public void actionPerformed(ActionEvent e){
        String sNum1 = oper1.getText();
        String sNum2 = oper2.getText();
        float iNum1 = Float.parseFloat(sNum1);
        float iNum2 = Float.parseFloat(sNum2);
        if(e.getSource() == suma){
            sNum1 = Float.toString(iNum1 + iNum2);
            resultadoFinal.setText("= " + sNum1);
        }
        if(e.getSource() == resta){
            sNum1 = Float.toString(iNum1 - iNum2);
            resultadoFinal.setText("= " + sNum1);
        }
        if(e.getSource() == multiplicacion){
            sNum1 = Float.toString(iNum1 * iNum2);
            resultadoFinal.setText("= " + sNum1);
        }
        if(e.getSource() == division){
            sNum1 = Float.toString(iNum1 / iNum2);
            resultadoFinal.setText("= " + sNum1);
        }

    }
    public static void main(String args[]){
        new Problema4();
    }
}