import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.event.ActionListener;

public class Problema3 extends JFrame implements ActionListener{
    JButton calcular;
    JLabel restante;
    JTextField edad;
    Container c;
    public Problema3(){
        c = getContentPane();
        calcular = new JButton("Calcular");
        restante = new JLabel("**********");
        edad = new JTextField();
        calcular.addActionListener(this);
        c.setLayout(new BorderLayout());
        add("North",edad);
        add("West",calcular);
        add("South",restante);
        setSize(500,100);
        setVisible(true);
    }
    public void actionPerformed(ActionEvent e){
        String sEdad;
        sEdad = edad.getText();
        int eRestante = 77 - Integer.parseInt(sEdad);
        sEdad = Integer.toString(eRestante);
        restante.setText(sEdad);
    }
    public static void main(String args[]){
        new Problema3();
    }
}
