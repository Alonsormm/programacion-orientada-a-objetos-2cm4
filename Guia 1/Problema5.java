import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Problema5 extends JFrame implements ActionListener{
    JTextField num1,num2,num3;
    JLabel numMayor;
    JButton mayor;
    Container c;
    public Problema5(){
        c = getContentPane();
        num1 = new JTextField();
        num2 = new JTextField();
        num3 = new JTextField();
        numMayor = new JLabel("********");
        mayor = new JButton("Cual es mayor?");
        mayor.addActionListener(this);
        c.setLayout(new BorderLayout());
        add("North", num1);
        add("Center", num2);
        add("South", num3);
        add("West",mayor);
        add("East", numMayor);
        setSize(500,100);
        setVisible(true);
    }
    public void actionPerformed(ActionEvent e){
        String sNum1,sNum2,sNum3;
        sNum1 = num1.getText();
        sNum2 = num2.getText();
        sNum3 = num3.getText();
        int iNum[] = new int[3];
        iNum[0] = Integer.parseInt(sNum1);
        iNum[1] = Integer.parseInt(sNum2);
        iNum[2] = Integer.parseInt(sNum3);
        int mayorS = iNum[0];
        for(int i = 1; i < 3 ; i++)
            if(mayorS<iNum[i])
                mayorS = iNum[i];
        sNum1 = Integer.toString(mayorS);
        numMayor.setText(sNum1);
    }
    public static void main(String args[]){
        new Problema5();
    }
}