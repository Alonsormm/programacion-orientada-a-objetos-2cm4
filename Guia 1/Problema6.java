import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class Problema6 extends JFrame implements ActionListener{
    JButton manzana, limon, naranja;
    JLabel Ffinal;
    Container c;
    public Problema6(){
        c = getContentPane();
        manzana = new JButton("Manzana");
        naranja = new JButton("Naranja");
        limon = new JButton("Limon");
        Ffinal = new JLabel("************");
        manzana.addActionListener(this);
        naranja.addActionListener(this);
        limon.addActionListener(this);
        c.setLayout(new BorderLayout());
        add("West", manzana);
        add("Center", naranja);
        add("East", limon);
        add("South", Ffinal);
        setSize(500,100);
        setVisible(true);
    }
    public void actionPerformed(ActionEvent e){
        if(e.getSource() == manzana)
            Ffinal.setText("Manzana");
        if(e.getSource() == naranja) 
            Ffinal.setText("Naranja");
        if(e.getSource() == limon)
            Ffinal.setText("Limon");
    }
    public static void main(String args[]){
        new Problema6();
    }
}