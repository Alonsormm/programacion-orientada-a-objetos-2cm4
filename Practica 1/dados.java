import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.event.ActionEvent;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;


class dados extends JFrame implements ActionListener{
    JButton jugar;
    JLabel ganar, dado1, dado2;
    ImageIcon dadoCara[];
    Container c;
    java.net.URL imgUrl;
    public dados(){
        c = getContentPane();

        
        dadoCara = new ImageIcon[6];
        imgUrl = getClass().getResource("dado1.png");
        dadoCara[0] = new ImageIcon(imgUrl);

        imgUrl = getClass().getResource("dado2.png");
        dadoCara[1] = new ImageIcon(imgUrl);

        imgUrl = getClass().getResource("dado3.png");
        dadoCara[2] = new ImageIcon(imgUrl);

        imgUrl = getClass().getResource("dado4.png");
        dadoCara[3] = new ImageIcon(imgUrl);

        imgUrl = getClass().getResource("dado5.png");
        dadoCara[4] = new ImageIcon(imgUrl);

        imgUrl = getClass().getResource("dado6.png");
        dadoCara[5] = new ImageIcon(imgUrl);



        ganar = new JLabel("*****");
        jugar = new JButton("Jugar");
        jugar.addActionListener(this);
        dado1 = new JLabel();
        dado2 = new JLabel();
        c.setLayout(new BorderLayout());
        c.add("North",jugar);
        c.add("West", dado1);
        c.add("East", dado2);
        c.add("South",ganar);
        setSize(500,300);
        setVisible(true);
    }
    public void actionPerformed(ActionEvent e){
        Font fonte = new Font("Courier", Font.BOLD,32);
        ganar.setFont(fonte);
        int randomNum1, randomNum2;
        randomNum1 = ThreadLocalRandom.current().nextInt(0, 6);
        dado1.setIcon(dadoCara[randomNum1]);
        randomNum2 = ThreadLocalRandom.current().nextInt(0, 6);
        dado2.setIcon(dadoCara[randomNum2]);
        if(randomNum1 + randomNum2 == 5){
            ganar.setText("Ganasteeeeee");
        }
        else{
            ganar.setText("Perdiste :C F");
        }
    }
    public static void main(String[] args) {
        new dados();
    }
}